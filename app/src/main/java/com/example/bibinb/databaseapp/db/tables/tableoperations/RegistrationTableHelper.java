package com.example.bibinb.databaseapp.db.tables.tableoperations;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.bibinb.databaseapp.db.DataBaseHelper;
import com.example.bibinb.databaseapp.db.tables.AppTables;
import com.example.bibinb.databaseapp.db.tables.coloumsnamesInterfaces.RegistrationCol;
import com.example.bibinb.databaseapp.models.RegistrationDetails;

import java.util.ArrayList;

/**
 * Created by bibin.b on 3/22/2017.
 */

public class RegistrationTableHelper implements RegistrationCol {


    // ===========================================================
    // ===========================================================
    // Method ,Insertion on table
    // ===========================================================
    // ===========================================================

    public void insertData(Context context, RegistrationDetails registrationDetails) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context, null, null, 1);


        // ===========================================================
        // SQLiteDatabase for for write and read from database
        // ===========================================================
        SQLiteDatabase database = dataBaseHelper.getWritableDatabase();


        // ===========================================================
        //ContentValues,used to add to values to coloum
        // ===========================================================
        ContentValues values = new ContentValues();
        values.put(COL_FIRST, registrationDetails.getFirst_name());
        values.put(COL_LAST, registrationDetails.getLast_name());
        values.put(COL_EMAIL, registrationDetails.getEmail());
        values.put(COL_COUNTRY, registrationDetails.getCountry());
        values.put(COL_GENDER, registrationDetails.getGender());
        values.put(COL_CITY, registrationDetails.getCity());
        values.put(COL_MOBILE, registrationDetails.getMobile());


        // ===========================================================
        // Insert to database
        // ===========================================================
        database.insert(AppTables.RegistrationTable.TABLE_NAME, null, values);


        // ===========================================================
        // Closing database connection
        // ===========================================================
        database.close();

    }


    // ===========================================================
    // ===========================================================
    // Method ,selection all data
    // ===========================================================
    // ===========================================================


    public ArrayList<RegistrationDetails> getAllDetails(Context context) {
        ArrayList<RegistrationDetails> registrationDetailses = new ArrayList<>();
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context, null, null, 1);
        SQLiteDatabase database = dataBaseHelper.getReadableDatabase();

        // ===========================================================
        // Query ,For get all data.Only give table name others are give null for all selection
        // ===========================================================
        String TABLE_NAME = AppTables.RegistrationTable.TABLE_NAME;

        Cursor cursor = database.query(TABLE_NAME, null, null, null, null, null, null);
        assert cursor != null;
        if (cursor.moveToFirst()) {
            do {
                RegistrationDetails details = new RegistrationDetails();

                //fisrname set to details(get by string(getstring) from columname(cursor.getColumIndex()colname))
                details.setFirst_name(cursor.getString(cursor.getColumnIndex(COL_FIRST)));
                details.setLast_name(cursor.getString(cursor.getColumnIndex(COL_LAST)));
                details.setEmail(cursor.getString(cursor.getColumnIndex(COL_EMAIL)));
                details.setCity(cursor.getString(cursor.getColumnIndex(COL_CITY)));
                details.setGender(cursor.getString(cursor.getColumnIndex(COL_GENDER)));
                details.setMobile(cursor.getString(cursor.getColumnIndex(COL_MOBILE)));
                details.setCountry(cursor.getString(cursor.getColumnIndex(COL_COUNTRY)));
                registrationDetailses.add(details);

            } while (cursor.moveToNext());

        }
        cursor.close();
        database.close();
        return registrationDetailses;

    }


    // ===========================================================
    // ===========================================================
    // Method ,Get Single Data (selection by arguments)
    // ===========================================================
    // ===========================================================
    public RegistrationDetails getSinglePersonData(Context context, final String mobilenumber) {
        RegistrationDetails details = new RegistrationDetails();
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context, null, null, 1);
        SQLiteDatabase database = dataBaseHelper.getReadableDatabase();


        // ===========================================================
        // Table Name
        // ===========================================================
        String TABLE_NAME = AppTables.RegistrationTable.TABLE_NAME;

        // ===========================================================
        // Projection (we need colum for the projection),here i want allcoloums details
        // ===========================================================
        String[] COLUMNS = {COL_FIRST, COL_LAST, COL_CITY, COL_MOBILE, COL_GENDER, COL_EMAIL, COL_COUNTRY};

        // ===========================================================
        //Here we use mobile number to find the person,(Two values give case  COL_MOBILE + "= ? or " + COL_NAME + "=?";
        // ===========================================================
        String SELECTION = COL_MOBILE + "=?";

        // ===========================================================
        // Selection arguments(mobile number use )
        // ===========================================================
        String[] SELECTION_ARG = {mobilenumber};

        // ===========================================================
        //
        // ===========================================================
        Cursor cursor = database.query(TABLE_NAME, COLUMNS, SELECTION, SELECTION_ARG, null, null, null);
        assert cursor != null;
        if (cursor.moveToFirst()) {
            do {
                //fisrname set to details(get by string(getstring) from columname(cursor.getColumIndex()colname))
                details.setFirst_name(cursor.getString(cursor.getColumnIndex(COL_FIRST)));
                details.setLast_name(cursor.getString(cursor.getColumnIndex(COL_LAST)));
                details.setEmail(cursor.getString(cursor.getColumnIndex(COL_EMAIL)));
                details.setCity(cursor.getString(cursor.getColumnIndex(COL_CITY)));
                details.setGender(cursor.getString(cursor.getColumnIndex(COL_GENDER)));
                details.setMobile(cursor.getString(cursor.getColumnIndex(COL_MOBILE)));
                details.setCountry(cursor.getString(cursor.getColumnIndex(COL_COUNTRY)));

            } while (cursor.moveToNext());
        }
        cursor.close();
        database.close();
        Log.d("getSinglePersonData", "getSinglePersonData: " + details.getEmail());
        return details;
    }


    // ===========================================================
    // ===========================================================
    // Method , Update Details of Already Exits
    // ===========================================================
    // ===========================================================
    public void updateDetails(Context context, RegistrationDetails registrationDetails, final String mobilenumber) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context, null, null, 1);
        SQLiteDatabase database = dataBaseHelper.getWritableDatabase();

        //update only 2 details with respect to mobile number
        ContentValues values = new ContentValues();
        values.put(COL_COUNTRY, registrationDetails.getCountry());
        values.put(COL_CITY, registrationDetails.getCity());

        String TABLE_NAME = AppTables.RegistrationTable.TABLE_NAME;
        String SELECTION = COL_MOBILE + "=? ";
        String[] SELECTION_ARG = {mobilenumber};

        database.update(TABLE_NAME, values, SELECTION, SELECTION_ARG);
        database.close();

    }


    // ===========================================================
    // ===========================================================
    // Method , Delete Details of Already Exits
    // ===========================================================
    // ===========================================================

    public void deleteDetails(Context context, String mobilenumber, String email) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context, null, null, 1);
        SQLiteDatabase database = dataBaseHelper.getWritableDatabase();
        String TABLE_NAME = AppTables.RegistrationTable.TABLE_NAME;
        String SELECTION = COL_MOBILE + "=? or " + COL_EMAIL + "=?";
        String[] SELECTION_ARGS = {mobilenumber, email};
        database.delete(TABLE_NAME, SELECTION, SELECTION_ARGS);
        database.close();

    }


    // ===========================================================
    // ===========================================================
    // Method ,Get Count of Tables Details
    // ===========================================================
    // ===========================================================

    public int getCounts(Context context) {
        int COUNT = 0;
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context, null, null, 1);
        SQLiteDatabase database = dataBaseHelper.getReadableDatabase();
        String TABLE_NAME = AppTables.RegistrationTable.TABLE_NAME;
        Cursor cursor = database.query(TABLE_NAME, null, null, null, null, null, null);
        assert cursor != null;
        COUNT = cursor.getCount();
        cursor.close();
        return COUNT;
    }


}
