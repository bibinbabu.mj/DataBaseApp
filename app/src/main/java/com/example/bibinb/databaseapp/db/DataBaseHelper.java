package com.example.bibinb.databaseapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.bibinb.databaseapp.db.tables.AppTables;

/**
 * Created by bibin.b on 3/22/2017.
 */

public class DataBaseHelper extends SQLiteOpenHelper {


    // ===========================================================
    // Constants
    // ===========================================================

    private static final String TAG_NAME = "DataBaseHelper";
    private static final String DB_NAME = "databaseapp.db";
    private static final int VERSION = 1;


    // ===========================================================
    //Constructors
    // ===========================================================
    public DataBaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DB_NAME, factory, VERSION);
    }


    // ===========================================================
    // Called when the database is created for the
    // first time. This is where the creation of
    // tables and the initial population of the tables should happen.
    // ===========================================================
    @Override
    public void onCreate(SQLiteDatabase db) {

        // ===========================================================
        //TABLE CREATION
        // ===========================================================
        db.execSQL(AppTables.RegistrationTable.CREATE_TABLES);

    }


    // ===========================================================
    // Called when the database needs to be upgraded.
    // The implementation should use this method to
    // drop tables, add tables, or do anything else
    // it needs to upgrade to the new schema version.
    // ===========================================================
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


        //   db.execSQL("DROP TABLE IF EXISTS " + TABLENAME);

        // ===========================================================
        // Create new table after dropping
        // ===========================================================
        //   onCreate(db);
    }
}
