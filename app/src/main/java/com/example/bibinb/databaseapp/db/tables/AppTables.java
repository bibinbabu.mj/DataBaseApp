package com.example.bibinb.databaseapp.db.tables;

import com.example.bibinb.databaseapp.db.tables.coloumsnamesInterfaces.ColumConstants;
import com.example.bibinb.databaseapp.db.tables.coloumsnamesInterfaces.RegistrationCol;

/**
 * Created by bibin.b on 3/22/2017.
 */

public class AppTables implements ColumConstants {

    // ===========================================================
    // Class (tables name)
    //Implement interface of coloums name
    // ===========================================================

    public static class RegistrationTable implements RegistrationCol {

        // ===========================================================
        //Constants,Create table name
        // ===========================================================
        public static final String TABLE_NAME = "RegistrationTable";


        //=============================================================
        //  String for table creation
        //=============================================================
        public static final String CREATE_TABLES = CREATE_TABLE + TABLE_NAME + OPEN_BRACE
                + _ID + SPACE + INTEGER + SPACE + PRIMARY_KEY + SPACE + AUTO_INCREMENT + COMA
                + COL_FIRST + SPACE + TEXT + COMA
                + COL_LAST + SPACE + TEXT + COMA
                + COL_EMAIL + SPACE + TEXT + COMA
                + COL_COUNTRY + SPACE + TEXT + COMA
                + COL_GENDER + SPACE + TEXT + COMA
                + COL_MOBILE + SPACE + INTEGER + COMA
                + COL_CITY + SPACE + TEXT
                + CLOSE_BRACE;
    }

    //Next Class for Table
    // ===========================================================
    // Class (tables name)
    //Implement interface of coloums name
    // ===========================================================


}
