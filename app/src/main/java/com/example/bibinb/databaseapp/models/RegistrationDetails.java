package com.example.bibinb.databaseapp.models;

/**
 * Created by bibin.b on 3/22/2017.
 */

public class RegistrationDetails {


    // ===========================================================
    // String initalization
    // ===========================================================

    String first_name;
    String last_name;
    String email;
    String mobile;
    String gender;
    String city;
    String country;


    // ===========================================================
    // Getter & Setter
    // Methods
    // ===========================================================
    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
