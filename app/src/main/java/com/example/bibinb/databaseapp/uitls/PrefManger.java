package com.example.bibinb.databaseapp.uitls;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;

/**
 * Created by bibin.b on 3/22/2017.
 */

public class PrefManger {

    // ===========================================================
    // Constants
    // ===========================================================

    private static final String TAG_NAME = "databaseapp.prefs";
    private static Preference instance;
    private SharedPreferences prefs;




    // ===========================================================
    // Constructors
    // ===========================================================

    private PrefManger(Context context) {
        prefs = context.getSharedPreferences(TAG_NAME, Context.MODE_PRIVATE);
    }

    public static Preference getInstance(Context context) {
        if (instance == null) {
            instance = new Preference(context);
        }
        return instance;
    }




    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    private void putString(String key, String value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private void putInt(String key, int value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void putLong(String key, long value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(key, value);
        prefs.edit();
    }

    private void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    private void putFloat(String key, float value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public static Preference getInstance() {
        return instance;
    }




    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public void setLogin(boolean value) {
        putBoolean("login", value);

    }

    public boolean isLogin() {
        return prefs.getBoolean("login", false);
    }


}
