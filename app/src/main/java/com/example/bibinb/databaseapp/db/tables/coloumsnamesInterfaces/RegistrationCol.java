package com.example.bibinb.databaseapp.db.tables.coloumsnamesInterfaces;

import android.provider.BaseColumns;

/**
 * Created by bibin.b on 3/22/2017.
 */

public interface RegistrationCol extends BaseColumns {
    String COL_FIRST = "first_name";
    String COL_LAST = "last_name";
    String COL_EMAIL = "email";
    String COL_MOBILE = "mobile";
    String COL_GENDER = "gender";
    String COL_CITY = "city";
    String COL_COUNTRY = "country";


}
