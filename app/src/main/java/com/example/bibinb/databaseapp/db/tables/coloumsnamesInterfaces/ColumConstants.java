package com.example.bibinb.databaseapp.db.tables.coloumsnamesInterfaces;

/**
 * Created by bibin.b on 3/22/2017.
 */

public interface ColumConstants {
    String TEXT = "TEXT";
    String INTEGER = "INTEGER";
    String BOOLEAN = "BOOLEAN";
    String DATE = "DATE";
    String DATETIME = "DATETIME";
    String FLOAT = "FLOAT";
    String DOUBLE = "DOUBLE";
    String PRIMARY_KEY = "PRIMARY KEY";
    String NOT_NULL = "NOT NULL";
    String DEFAULT = "DEFAULT";
    String UNIQUE = "UNIQUE";
    String CREATE_TABLE = "CREATE TABLE ";
    String AUTO_INCREMENT = " AUTOINCREMENT";
    String COMA=",";
    String SPACE=" ";
    String OPEN_BRACE="(";
    String CLOSE_BRACE=");";
}
